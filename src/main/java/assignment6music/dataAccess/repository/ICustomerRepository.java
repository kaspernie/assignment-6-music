package assignment6music.dataAccess.repository;

import assignment6music.dataAccess.model.Customer;
import assignment6music.dataAccess.model.CustomerCountry;
import assignment6music.dataAccess.model.CustomerGenre;
import assignment6music.dataAccess.model.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ICustomerRepository {

    // returns a list of all customers
    public ArrayList<Customer> getAllCustomers();

    //gets one customer by id
    public Customer getCustomerById(int customerId);

    //gets one customer by name
    public Customer getCustomerByName(String firstName, String lastName);

    // returns a list of all customers with offset and limit
    public ArrayList<Customer> getSubsetOfCustomers(Integer offset, Integer limit);

    // Add new Customer
    public Boolean addCustomer(Customer customer);

    // returns number of customers per country
    public ArrayList<CustomerCountry> getCustomersPerCountry();

    // returns top20 high spenders
    public ArrayList<CustomerSpender> getCustomerSpender();

    //returns most popular genre(s) for a given customer
    public ArrayList<CustomerGenre> getCustomerFavouriteGenre(int customerId);

    // update existing customer
    Boolean updateCustomer(int customerId, Customer customer);

}
