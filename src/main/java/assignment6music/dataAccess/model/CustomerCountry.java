package assignment6music.dataAccess.model;

public class CustomerCountry {
    public int numCustomers;
    public String Country;

    public CustomerCountry(int numCustomers, String country) {
        this.numCustomers = numCustomers;
        this.Country = country;
    }
}
