package assignment6music.controller;

import assignment6music.dataAccess.model.Customer;
import assignment6music.dataAccess.model.CustomerCountry;
import assignment6music.dataAccess.model.CustomerGenre;
import assignment6music.dataAccess.model.CustomerSpender;
import assignment6music.dataAccess.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
public class CustomerController {

    private final ICustomerRepository customerRepository;

    public CustomerController(@Autowired ICustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/api")
    public String apiIndex() {
        return "this is api root page";
    }

    @GetMapping("/api/customer")
    public ArrayList<Customer> apiAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/api/customer/{customerId}")
    public Customer apiCustomerById(
            @PathVariable int customerId
    ) {
        return customerRepository.getCustomerById(customerId);
    }

    @GetMapping("/api/customer/{firstName}/{lastName}")
    public Customer apiCustomerByName(
            @PathVariable String firstName,
            @PathVariable String lastName
    ) {
        return customerRepository.getCustomerByName(firstName, lastName);
    }

    @GetMapping(value = "/api/customer/subset/{offset}/{limit}")
    public ArrayList<Customer> apiCustomersWithLimitAndOffset(
            @PathVariable int offset,
            @PathVariable int limit
    ) {
        return customerRepository.getSubsetOfCustomers(offset, limit);
    }

    @PostMapping("/api/customer/add")
    public Boolean apiAddNewCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @PatchMapping("/api/customer/{customerId}/update")
    public Boolean apiUpdateCustomer(
            @PathVariable int customerId,
            @RequestBody Customer customer
    ) {
        return customerRepository.updateCustomer(
                customerId, customer);
    }


    @GetMapping("/api/customer/country/count")
    public ArrayList<CustomerCountry> apiCustomersByCountry() {
        return customerRepository.getCustomersPerCountry();
    }

    @GetMapping("api/customer/spender")
    public ArrayList<CustomerSpender> apiCustomersBigSpenders() {
        return customerRepository.getCustomerSpender();
    }

    @GetMapping("api/customer/{customerId}/genre")
    public ArrayList<CustomerGenre> apiCustomerFavouriteGenre(@PathVariable int customerId) {

        return customerRepository.getCustomerFavouriteGenre(customerId);
    }

}
