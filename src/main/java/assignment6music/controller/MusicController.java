package assignment6music.controller;

import assignment6music.dataAccess.model.RandomInfoObject;
import assignment6music.dataAccess.model.TrackInfo;
import assignment6music.dataAccess.repository.IMusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class MusicController {

    private final IMusicRepository musicRepository;

    public MusicController(@Autowired IMusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    @GetMapping("/")
    public String index(Model model) {
        ArrayList<String> artists = musicRepository.getRandomArtists();
        ArrayList<String> songs = musicRepository.getRandomSongs();
        ArrayList<String> genres = musicRepository.getRandomGenres();

        RandomInfoObject randomInfoObject = new RandomInfoObject(artists, songs, genres);
        model.addAttribute("randomInfoObject", randomInfoObject);
        return "index";
    }

    @GetMapping("/search")
    public String search(@RequestParam String query, Model model) {
        ArrayList<TrackInfo> results = musicRepository.getInfoByTrackName(query);
        model.addAttribute("results", results);
        return "search";
    }



}
