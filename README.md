# Not iTunes DB

This is the second Java assignment in Noroff's Java module. See [Assignment_2_Java_Data_access_and_display.pdf](Assignment_2_Java_Data_access_and_display.pdf).
It has a website that shows "random" music information and a search functionality to find songs from a database. It also includes a REST API with various endpoints for getting customer information.


## Usage

The app is deployed live at:
[not-itunes.herokuapp.com](https://not-itunes.herokuapp.com)

For testing API endpoint see [not_itunes.postman_collection.json](not_itunes.postman_collection.json).

If you want to make your own Docker image of this repo, first clone the repository and then build the image (replace `<myTag>` with [unique heroku] app name):
```
git clone git@gitlab.com:Nsknielsen/assignment-6-music.git
cd assignment-6-music
docker build -t <myTag> .
```
(Docker Desktop is required on MacOS and Windows)

To deploy to Heroku, run the following commands (replace `<myTag>` with unique heroku app name):
```
heroku container:login
docker tag <myTag> registry.heroku.com/<myTag>/web
docker push registry.heroku.com/<myTag>/web
heroku container:release web -a <myTag>
```

For running the app from http://localhost:8080, open the project in e.g. IntelliJ and build it after changing line 5 in `java/assignment6musci/dataAcces/dbConnection/ConnectionUrlHelper`

from:

 `static final public String dbUrl = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";`

to:

 `static final public String dbUrl = "jdbc:sqlite:src/main/resources/Chinook_Sqlite.sqlite";`



## Maintainers

Kasper Nielsen @kaspernie

Nikolaj Nielsen @Nsknielsen
